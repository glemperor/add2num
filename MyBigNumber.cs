﻿using System;

namespace Add2Num
{
    class MyBigNumber
    {
        static void Main(string[] args)
        {

            Console.WriteLine(sum("12345", "345"));
        }

         public static string sum(string firstNum, string secondNum)
        {
            if (firstNum.Length > secondNum.Length)
            {
                firstNum = firstNum + secondNum;
                secondNum = firstNum.Substring(0, firstNum.Length - secondNum.Length);
                firstNum = firstNum.Substring(secondNum.Length);
            }

            string storedString = "";

            // reverse 2 strings
            char[] ch = firstNum.ToCharArray();
            Array.Reverse(ch);
            firstNum = new string(ch);
            char[] ch1 = secondNum.ToCharArray();
            Array.Reverse(ch1);
            secondNum = new string(ch1);

            int rememberedNum = 0;
            for (int i = 0; i < firstNum.Length; i++)
            {
             
                int sum = ((int)(firstNum[i] - '0') +
                        (int)(secondNum[i] - '0') + rememberedNum);
                storedString += (char)(sum % 10 + '0');
                Console.WriteLine();
                // calculate remembered num for next step
                rememberedNum = sum / 10;
            }

            for (int i = firstNum.Length; i < secondNum.Length; i++)
            {
                int sum = ((int)(secondNum[i] - '0') + rememberedNum);
                storedString += (char)(sum % 10 + '0');
                rememberedNum = sum / 10;
            }

            if (rememberedNum > 0)
                storedString += (char)(rememberedNum + '0');

            // reverse result
            char[] ch2 = storedString.ToCharArray();
            Array.Reverse(ch2);
            storedString = new string(ch2);

            return storedString;
        }
    } 
}

